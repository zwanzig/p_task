/**
 * Example problem with existing solution and passing test.
 * See problem 0 in the spec file for the assertion
 * @returns {string}
 */
exports.example = () => 'hello world';

exports.stripPrivateProperties = (propsToExclude, data) => {
  const localData = [...data];

  return localData.map((obj) => {
    // It's bad from performance perspective, but readable
    propsToExclude.forEach((prop) => {
      // eslint-disable-next-line
      delete obj[prop];
    });

    return obj;
  });
};

exports.excludeByProperty = (prop, obj) => {
  const hasProp = Object.prototype.hasOwnProperty;

  return obj.filter(it => !hasProp.call(it, prop));
};

exports.sumDeep = arr => arr.map((item) => {
  const { val } = item.objects
    .reduce((prev, curr) => ({ val: prev.val + curr.val }));

  return { objects: val };
});

exports.applyStatusColor = (definitions, data) => {
  const helper = {};
  const results = [];
  const colors = Object.keys(definitions);

  // Convert to easily accesible data
  colors.forEach((color) => {
    const statues = definitions[color];
    statues.forEach((st) => {
      helper[st] = color;
    });
  });

  data.forEach((it) => {
    const { status } = it;
    const color = helper[status];

    if (color) results.push({ status, color });
  });

  return results;
};

exports.createGreeting = (greet, greeting) => name => `${greet(greeting, name)}`;

exports.setDefaults = (props) => {
  // Assumed that props are 1 level deep only
  // function should be extended in other case
  const propsNames = Object.keys(props);

  return (obj) => {
    const localObj = { ...obj };

    propsNames.forEach((prop) => {
      const hasProp = Object.prototype.hasOwnProperty.call(localObj, prop);

      if (!hasProp) {
        localObj[prop] = props[prop];
      }
    });

    return localObj;
  };
};

exports.fetchUserByNameAndUsersCompany = (username, services) => new Promise(async (resolve) => {
  const [users, status] = await Promise.all([
    services.fetchUsers(),
    services.fetchStatus(),
  ]);

  const user = users.find(us => us.name === username);
  const company = await services.fetchCompanyById(user.companyId);

  resolve({ company, status, user });
});
